package co.jbink.mustacheee.inventory;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.icu.util.GregorianCalendar;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.unmarshallers.IntegerSetUnmarshaller;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

public class CheckoutActivity extends AppCompatActivity {

    private static final String TAG = CheckoutActivity.class.getSimpleName();
    private DecoratedBarcodeView barcodeView;
    private BeepManager beepManager;
    private String entityId;
    private Button proceedBtn, rescanBtn;
    private EditText userInput;
    public CognitoCachingCredentialsProvider credentialsProvider;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if(result.getText() == null){
                //prevent duplicate scans
                return;
            }

            beepManager.playBeepSoundAndVibrate();
            entityId = result.getText();
            barcodeView.setStatusText("Scanned : " + entityId + "\nContinue?");
            proceedBtn.setVisibility(View.VISIBLE);
            rescanBtn.setVisibility(View.VISIBLE);;
            pause(barcodeView.getRootView());
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        this.barcodeView = (DecoratedBarcodeView) findViewById(R.id.barcode_scanner);
        barcodeView.decodeContinuous(callback);
        this.beepManager = new BeepManager(this);
        this.proceedBtn = (Button) findViewById(R.id.proceedBtn);
        proceedBtn.setOnClickListener(checkoutStage1);
        this.rescanBtn = (Button) findViewById(R.id.rescanBtn);
        this.userInput = (EditText) findViewById(R.id.userInput);


        this.credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:a8252284-deb3-4711-a3ea-9ee220fee452",
                Constants.REGION
        );
        CognitoSyncManager syncClient = new CognitoSyncManager(
                getApplicationContext(),
                Constants.REGION,
                credentialsProvider);
    }

    @Override
    protected void onResume(){
        super.onResume();
        barcodeView.resume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        barcodeView.pause();
    }

    public void pause(View view){
        barcodeView.pause();
    }

    public void resume(View view){
        barcodeView.resume();
    }

    public void triggerScan(View view){
        barcodeView.decodeSingle(callback);
    }

    public void proceed(View view){
        this.userInput.setVisibility(View.VISIBLE);
        proceedBtn.setOnClickListener(checkoutStage2);
    }

    public void rescan(View view){
        if(this.rescanBtn.isShown())
            this.rescanBtn.setVisibility(View.GONE);
        if(this.proceedBtn.isShown())
            this.proceedBtn.setVisibility(View.GONE);
        if(this.userInput.isShown()){
            this.userInput.setText("");
            this.userInput.setVisibility(View.GONE);
        }
        proceedBtn.setOnClickListener(checkoutStage1);
        barcodeView.setStatusText("");
        barcodeView.resume();
    }

    private View.OnClickListener checkoutStage2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String input = userInput.getText().toString();
            if(input.equals("")) {
                barcodeView.setStatusText("Please enter a name or id of who is checking out object.");
                return;
            }
            barcodeView.setStatusText("Successfully checked out item '" + entityId + "' to '" + input +
                    "'\nScan another object to continue");
            proceedBtn.setVisibility(View.GONE);
            rescanBtn.setVisibility(View.GONE);
            //userInput.setText("");
            userInput.setVisibility(View.GONE);
            view.setOnClickListener(checkoutStage1);
            barcodeView.resume();
            SQLiteDatabase db = new InventoryDBHelper(view.getContext()).getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(InventoryContract.InventoryEntry.COLUMN_ID, entityId);
            values.put(InventoryContract.InventoryEntry.COLUMN_UUID, "281-330-8004");
            values.put(InventoryContract.InventoryEntry.COLUMN_DEVICE, "Dell Laptop");
            values.put(InventoryContract.InventoryEntry.COLUMN_DEPARTMENT, "Amazement Center");
            values.put(InventoryContract.InventoryEntry.COLUMN_LOCATION, "Storage Locker");
            values.put(InventoryContract.InventoryEntry.COLUMN_ASSIGNED_TO, input);
            values.put(InventoryContract.InventoryEntry.COLUMN_ASSIGNED_BY, "Super User");
            values.put(InventoryContract.InventoryEntry.COLUMN_UPDATED_AT, System.currentTimeMillis());
            if(db.query(InventoryContract.InventoryEntry.TABLE_NAME, null, "id = ?",new String[]{entityId},null,null,null).getCount() > 0){
                db.update(InventoryContract.InventoryEntry.TABLE_NAME, values, "id = ?", new String[]{entityId});
            }else{
                db.insert(InventoryContract.InventoryEntry.TABLE_NAME, null, values);
            }
            db.close();

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    String input = userInput.getText().toString();

                    Inventory item = new Inventory();
                    item.setId(entityId);
                    item.setUuid("281-330-8004");
                    item.setDevice("Dell Laptop");
                    item.setDepartment("Amazement Center");
                    item.setLocation("Storage Locker");
                    item.setAssignedTo(input);
                    item.setAssignedBy("SuperUser"); //get Login
                    item.setUpdatedAt(System.currentTimeMillis()+"");
                    CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                            getApplicationContext(),
                            "us-west-2:a8252284-deb3-4711-a3ea-9ee220fee452", // Identity Pool ID
                            Regions.US_WEST_2 // Region
                    );

                    AmazonDynamoDBClient ddbClient = Region.getRegion(Regions.US_WEST_2)
                            .createClient(AmazonDynamoDBClient.class, credentialsProvider, new ClientConfiguration());
                    DynamoDBMapper mapper = new DynamoDBMapper(ddbClient);
                    mapper.save(item);

                }
            };
                Thread myThread = new Thread(runnable);
            myThread.start();
        }
    };

    private View.OnClickListener checkoutStage1 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            proceed(view);
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
}

package co.jbink.mustacheee.inventory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ImportActivity extends AppCompatActivity {

    private String spreadSheetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import);

        Button importBtn = (Button) findViewById(R.id.import_importBtn);
        importBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = ((EditText) findViewById(R.id.import_spreadsheetId)).getText().toString();
                if(input.equals(""))
                    return;
                SpreadSheetParser spreadSheetParser = new SpreadSheetParser("https://spreadsheets.google.com/tq?key=" + spreadSheetId);
                spreadSheetParser.execute();
            }
        });
    }
}

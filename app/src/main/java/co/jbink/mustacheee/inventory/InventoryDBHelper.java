package co.jbink.mustacheee.inventory;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;

/**
 * Created by benja on 10/25/2016.
 */

public class InventoryDBHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "inventory.db";

    public InventoryDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_USER_TABLE = "CREATE TABLE " +
                InventoryContract.UserEntry.TABLE_NAME + " (" +
                InventoryContract.UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                InventoryContract.UserEntry.COLUMN_USERNAME + " TEXT NOT NULL, " +
                InventoryContract.UserEntry.COLUMN_PASSWORD + " TEXT NOT NULL);";

        final String SQL_CREATE_INVENTORY_TABLE = "CREATE TABLE " +
                InventoryContract.InventoryEntry.TABLE_NAME  + " (" +
                InventoryContract.InventoryEntry.COLUMN_ID +  " TEXT PRIMARY KEY, "+
                InventoryContract.InventoryEntry.COLUMN_UUID + " TEXT, " +
                InventoryContract.InventoryEntry.COLUMN_DEVICE + " TEXT, " +
                InventoryContract.InventoryEntry.COLUMN_DEPARTMENT + " TEXT, "+
                InventoryContract.InventoryEntry.COLUMN_LOCATION + " TEXT, " +
                InventoryContract.InventoryEntry.COLUMN_ASSIGNED_TO + " TEXT, " +
                InventoryContract.InventoryEntry.COLUMN_ASSIGNED_BY + " TEXT, " +
                InventoryContract.InventoryEntry.COLUMN_UPDATED_AT + " TEXT); ";
                //" FOREIGN KEY (" + InventoryContract.InventoryEntry.COLUMN_ASSIGNED_BY + ") REFERENCES " +
               // InventoryContract.UserEntry.TABLE_NAME + " (" + InventoryContract.UserEntry._ID + "));";

        //sqLiteDatabase.execSQL(SQL_CREATE_USER_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_INVENTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
       // sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + InventoryContract.UserEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + InventoryContract.InventoryEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public static long insertOrUpdateItem(Context context, String id, String uuid, String device, String department, String location, String assignedTo){
        ContentValues values = new ContentValues();
        values.put(InventoryContract.InventoryEntry.COLUMN_ID, id);
        values.put(InventoryContract.InventoryEntry.COLUMN_UUID, uuid);
        values.put(InventoryContract.InventoryEntry.COLUMN_DEVICE, device);
        values.put(InventoryContract.InventoryEntry.COLUMN_DEPARTMENT, department);
        values.put(InventoryContract.InventoryEntry.COLUMN_LOCATION, location);
        values.put(InventoryContract.InventoryEntry.COLUMN_ASSIGNED_TO, assignedTo);
        values.put(InventoryContract.InventoryEntry.COLUMN_ASSIGNED_BY, getUser());
        values.put(InventoryContract.InventoryEntry.COLUMN_UPDATED_AT, System.currentTimeMillis());

        SQLiteDatabase db = new InventoryDBHelper(context).getWritableDatabase();
        long result = db.insertWithOnConflict(InventoryContract.InventoryEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        return result;
    }

    public static String getUser(){
        return "Super User";
    }
}

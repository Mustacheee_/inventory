package co.jbink.mustacheee.inventory;

import android.os.AsyncTask;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by benja on 10/31/2016.
 */

public class SpreadSheetParser extends AsyncTask<Void, Void, String>{
    private String url;

    public SpreadSheetParser(String url){
        this.url = url;
    }

    @Override
    protected String doInBackground(Void... urls){
        try{
            String importString = downloadUrl(this.url);
            insertData(importString);
        }catch(Exception e){
            return "Unable to download requested webpage";
        }
        return "success";
    }

    @Override
    protected void onPostExecute(String result){
        int start = result.indexOf("{", result.indexOf("{") + 1);
        int end = result.lastIndexOf("}");
        String jsonResponse = result.substring(start, end);
        try{
            JSONObject table = new JSONObject(jsonResponse);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private boolean insertData(String importString){
        int start = importString.indexOf("{", importString.indexOf("{") + 1);
        int end = importString.lastIndexOf("}");
        String trimmedSting = importString.substring(start, end);
        try{
            JSONObject jsonString = new JSONObject(trimmedSting);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String downloadUrl(String urlString) throws IOException{
        InputStream is = null;

        try{
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000); //milliseconds
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            connection.connect();
            int responseCode = connection.getResponseCode();
            is = connection.getInputStream();

            return converStreamToString(is);
        }finally {
            if(is != null)
                is.close();
        }
    }

    private String converStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try{
            while((line = reader.readLine()) != null){
                sb.append(line);
                sb.append("\n");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                is.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}

package co.jbink.mustacheee.inventory;

import android.provider.BaseColumns;

/**
 * Created by benja on 10/25/2016.
 */

public class InventoryContract {
    public static final class InventoryEntry {
        public static final String TABLE_NAME = "inventory";

        public static final String COLUMN_ID = "id";

        public static final String COLUMN_UUID = "uuid";

        public static final String COLUMN_DEVICE = "device";

        public static final String COLUMN_DEPARTMENT = "department";

        public static final String COLUMN_LOCATION = "location";

        public static final String COLUMN_ASSIGNED_TO = "assigned_to";

        public static final String COLUMN_ASSIGNED_BY = "assigned_by";

        public static final String COLUMN_UPDATED_AT = "updated_at";
    }

    public static final class UserEntry implements BaseColumns{
        public static final String TABLE_NAME = "user";

        public static final String COLUMN_USERNAME = "username";

        public static final String COLUMN_PASSWORD = "password";
    }
}

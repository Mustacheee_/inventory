package co.jbink.mustacheee.inventory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.*;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;
import com.amazonaws.services.dynamodbv2.model.*;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Button scanbtn, inputDataBtn;
    private TextView formatTxt, contentTxt;
    CognitoCachingCredentialsProvider credentialsProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.scanbtn = (Button) findViewById(R.id.scan_button);
        this.inputDataBtn = (Button) findViewById(R.id.input_dataBtn);
        this.formatTxt = (TextView) findViewById(R.id.scan_format);
        this.contentTxt = (TextView) findViewById(R.id.scan_content);

        this.credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:a8252284-deb3-4711-a3ea-9ee220fee452",
                Regions.US_WEST_2
        );
        CognitoSyncManager syncClient = new CognitoSyncManager(
                getApplicationContext(),
                Constants.REGION,
                credentialsProvider);

        scanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, CheckoutActivity.class);
                startActivity(i);
            }
        });

        inputDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*SpreadSheetParser ssp = new SpreadSheetParser("https://spreadsheets.google.com/tq?key=1j_r6ZKoFZOvmz75XzyQoxaaquw74bpBkQPLF6O02lqY");
                ssp.execute();*/
                Intent i = new Intent(MainActivity.this, InputInventoryActivity.class);
                startActivity(i);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if(scanResult != null) {
            String content = scanResult.getContents();
            String format = scanResult.getFormatName();
            formatTxt.setText("Format: " + format);
            contentTxt.setText("Content: " + content);
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

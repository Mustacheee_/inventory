package co.jbink.mustacheee.inventory;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InputInventoryActivity extends AppCompatActivity {

    private String id, uuid, device, department, location, assignedTo;
    private Button enterBtn, retrieveBtn;
    private TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_inventory);

        Button enterBtn = (Button) findViewById(R.id.inputInventory_enterBtn);
        Button importBtn = (Button) findViewById(R.id.inputInventory_importBtn);
        this.responseText = (TextView) findViewById(R.id.inputInventory_response);

        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterData();
            }
        });

        importBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(InputInventoryActivity.this, ImportActivity.class);
                startActivity(i);
            }
        });
    }

    private void enterData(){
        this.id = ((EditText) findViewById(R.id.inputInventory_id)).getText().toString();
        this.uuid = ((EditText) findViewById(R.id.inputInventory_uuid)).getText().toString();
        this.device = ((EditText) findViewById(R.id.inputInventory_device)).getText().toString();
        this.department = ((EditText) findViewById(R.id.inputInventory_department)).getText().toString();
        this.location = ((EditText) findViewById(R.id.inputInventory_location)).getText().toString();
        this.assignedTo = ((EditText) findViewById(R.id.inputInventory_assignedTo)).getText().toString();

        long result = InventoryDBHelper.insertOrUpdateItem(getApplicationContext(), id, uuid, device, department, location, assignedTo);
        if(result != -1)
            responseText.setText("Successfully inserted item");
        else
            responseText.setText("Could not insert item");
    }
}

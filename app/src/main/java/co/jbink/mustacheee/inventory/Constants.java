package co.jbink.mustacheee.inventory;

import com.amazonaws.regions.Regions;

/**
 * Created by benja on 10/26/2016.
 */

public class Constants {

    public static final String ROLE_UNATHENTICATED = "InventoryManager_Unathenticated";

    public static final String ROLE_AUTHENTICATED = "InventoryManger_Authenticated";

    public static final String IDENTITY_POOL = "us-west-2:a8252284-deb3-4711-a3ea-9ee220fee452";

    public static final Regions REGION = Regions.US_WEST_2;

    public static final String APPID = "bcper6c6ggint8kouqqeoas14";

    public static final String BOOKS_RESOURCE_NAME = "arn:aws:dynamodb:us-west-2:708519917047:table/Books";

   /*

   // Initialize the Amazon Cognito credentials provider
    CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
            getApplicationContext(),
            "us-west-2:a8252284-deb3-4711-a3ea-9ee220fee452", // Identity Pool ID
            Regions.US_WEST_2 // Region
    );

    // Initialize the Cognito Sync client
    CognitoSyncManager syncClient = new CognitoSyncManager(
       getApplicationContext(),
       Regions.US_WEST_2, // Region
       credentialsProvider);

       // Create a record in a dataset and synchronize with the server
    Dataset dataset = syncClient.openOrCreateDataset("myDataset");
    dataset.put("myKey", "myValue");
    dataset.synchronize(new DefaultSyncCallback() {
    @Override
    public void onSuccess(Dataset dataset, List newRecords) {
        //Your handler code here
    }
});
    */
}
